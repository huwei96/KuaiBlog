﻿
using System;
namespace JinianNet.KuaiBlog.Entity
{
	/// <summary>
	/// DataStoreSettings:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class DataStoreSetting
	{
		public DataStoreSetting()
		{}
		#region Model
		private int _datastoresettingrowid;
		private Guid _blogid;
		private string _extensiontype;
		private string _extensionid;
		private string _settings;
		/// <summary>
		/// 
		/// </summary>
		public int DataStoreSettingRowId
		{
			set{ _datastoresettingrowid=value;}
			get{return _datastoresettingrowid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid BlogId
		{
			set{ _blogid=value;}
			get{return _blogid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ExtensionType
		{
			set{ _extensiontype=value;}
			get{return _extensiontype;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ExtensionId
		{
			set{ _extensionid=value;}
			get{return _extensionid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Settings
		{
			set{ _settings=value;}
			get{return _settings;}
		}
		#endregion Model

	}
}

