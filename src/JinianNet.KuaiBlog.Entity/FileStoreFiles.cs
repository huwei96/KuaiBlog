﻿
using System;
namespace JinianNet.KuaiBlog.Entity
{
	/// <summary>
	/// FileStoreFiles:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class FileStoreFiles
	{
		public FileStoreFiles()
		{}
		#region Model
		private Guid _fileid;
		private Guid _parentdirectoryid;
		private string _name;
		private string _fullpath;
		private byte[] _contents;
		private int _size;
		private DateTime _createdate;
		private DateTime _lastaccess;
		private DateTime _lastmodify;
		/// <summary>
		/// 
		/// </summary>
		public Guid FileId
		{
			set{ _fileid=value;}
			get{return _fileid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid ParentDirectoryId
		{
			set{ _parentdirectoryid=value;}
			get{return _parentdirectoryid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Name
		{
			set{ _name=value;}
			get{return _name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string FullPath
		{
			set{ _fullpath=value;}
			get{return _fullpath;}
		}
		/// <summary>
		/// 
		/// </summary>
		public byte[] Contents
		{
			set{ _contents=value;}
			get{return _contents;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int Size
		{
			set{ _size=value;}
			get{return _size;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime CreateDate
		{
			set{ _createdate=value;}
			get{return _createdate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime LastAccess
		{
			set{ _lastaccess=value;}
			get{return _lastaccess;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime LastModify
		{
			set{ _lastmodify=value;}
			get{return _lastmodify;}
		}
		#endregion Model

	}
}

