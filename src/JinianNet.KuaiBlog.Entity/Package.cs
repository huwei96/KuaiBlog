﻿
using System;
namespace JinianNet.KuaiBlog.Entity
{
	/// <summary>
	/// Packages:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class Package
	{
		public Package()
		{}
		#region Model
		private string _packageid;
		private string _version;
		/// <summary>
		/// 
		/// </summary>
		public string PackageId
		{
			set{ _packageid=value;}
			get{return _packageid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Version
		{
			set{ _version=value;}
			get{return _version;}
		}
		#endregion Model

	}
}

