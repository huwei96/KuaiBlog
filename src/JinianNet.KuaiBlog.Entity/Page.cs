﻿
using System;
namespace JinianNet.KuaiBlog.Entity
{
	/// <summary>
	/// Pages:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class Page
	{
		public Page()
		{}
		#region Model
		private int _pagerowid;
		private Guid _blogid;
		private Guid _pageid;
		private string _title;
		private string _description;
		private string _pagecontent;
		private string _keywords;
		private DateTime? _datecreated;
		private DateTime? _datemodified;
		private bool _ispublished;
		private bool _isfrontpage;
		private Guid _parent;
		private bool _showinlist;
		private string _slug;
		private bool _isdeleted= false;
		/// <summary>
		/// 
		/// </summary>
		public int PageRowId
		{
			set{ _pagerowid=value;}
			get{return _pagerowid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid BlogId
		{
			set{ _blogid=value;}
			get{return _blogid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid PageId
		{
			set{ _pageid=value;}
			get{return _pageid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Title
		{
			set{ _title=value;}
			get{return _title;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Description
		{
			set{ _description=value;}
			get{return _description;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string PageContent
		{
			set{ _pagecontent=value;}
			get{return _pagecontent;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Keywords
		{
			set{ _keywords=value;}
			get{return _keywords;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? DateCreated
		{
			set{ _datecreated=value;}
			get{return _datecreated;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? DateModified
		{
			set{ _datemodified=value;}
			get{return _datemodified;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool IsPublished
		{
			set{ _ispublished=value;}
			get{return _ispublished;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool IsFrontPage
		{
			set{ _isfrontpage=value;}
			get{return _isfrontpage;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid Parent
		{
			set{ _parent=value;}
			get{return _parent;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool ShowInList
		{
			set{ _showinlist=value;}
			get{return _showinlist;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Slug
		{
			set{ _slug=value;}
			get{return _slug;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool IsDeleted
		{
			set{ _isdeleted=value;}
			get{return _isdeleted;}
		}
		#endregion Model

	}
}

