﻿
using System;
namespace JinianNet.KuaiBlog.Entity
{
	/// <summary>
	/// PostNotify:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class PostNotify
	{
		public PostNotify()
		{}
		#region Model
		private int _postnotifyid;
		private Guid _blogid;
		private Guid _postid;
		private string _notifyaddress;
		/// <summary>
		/// 
		/// </summary>
		public int PostNotifyId
		{
			set{ _postnotifyid=value;}
			get{return _postnotifyid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid BlogId
		{
			set{ _blogid=value;}
			get{return _blogid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid PostId
		{
			set{ _postid=value;}
			get{return _postid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string NotifyAddress
		{
			set{ _notifyaddress=value;}
			get{return _notifyaddress;}
		}
		#endregion Model

	}
}

