﻿
using System;
namespace JinianNet.KuaiBlog.Entity
{
	/// <summary>
	/// Settings:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class Setting
	{
		public Setting()
		{}
		#region Model
		private int _settingrowid;
		private Guid _blogid;
		private string _settingname;
		private string _settingvalue;
		/// <summary>
		/// 
		/// </summary>
		public int SettingRowId
		{
			set{ _settingrowid=value;}
			get{return _settingrowid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid BlogId
		{
			set{ _blogid=value;}
			get{return _blogid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string SettingName
		{
			set{ _settingname=value;}
			get{return _settingname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string SettingValue
		{
			set{ _settingvalue=value;}
			get{return _settingvalue;}
		}
		#endregion Model

	}
}

