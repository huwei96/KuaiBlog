﻿
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;

namespace JinianNet.KuaiBlog.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:FileStoreFileThumbs
	/// </summary>
    public partial class FileStoreFileThumbs : BaseDAL
	{
		public FileStoreFileThumbs()
		{}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(JinianNet.KuaiBlog.Entity.FileStoreFileThumb model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update be_FileStoreFileThumbs set ");
			strSql.Append("FileId=@FileId,");
			strSql.Append("size=@size,");
			strSql.Append("contents=@contents");
			strSql.Append(" where thumbnailId=@thumbnailId ");
			SqlParameter[] parameters = {
					new SqlParameter("@FileId", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@size", SqlDbType.Int,4),
					new SqlParameter("@contents", SqlDbType.VarBinary,-1),
					new SqlParameter("@thumbnailId", SqlDbType.UniqueIdentifier,16)};
			parameters[0].Value = model.FileId;
			parameters[1].Value = model.size;
			parameters[2].Value = model.contents;
			parameters[3].Value = model.thumbnailId;

			int rows=Helper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(Guid thumbnailId)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from be_FileStoreFileThumbs ");
			strSql.Append(" where thumbnailId=@thumbnailId ");
			SqlParameter[] parameters = {
					new SqlParameter("@thumbnailId", SqlDbType.UniqueIdentifier,16)			};
			parameters[0].Value = thumbnailId;

			int rows=Helper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string thumbnailIdlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from be_FileStoreFileThumbs ");
			strSql.Append(" where thumbnailId in ("+thumbnailIdlist + ")  ");
			int rows=Helper.ExecuteNonQuery(CommandType.Text,strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public JinianNet.KuaiBlog.Entity.FileStoreFileThumb GetItem(Guid thumbnailId)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 thumbnailId,FileId,size,contents from be_FileStoreFileThumbs ");
			strSql.Append(" where thumbnailId=@thumbnailId ");
			SqlParameter[] parameters = {
					new SqlParameter("@thumbnailId", SqlDbType.UniqueIdentifier,16)			};
			parameters[0].Value = thumbnailId;

			JinianNet.KuaiBlog.Entity.FileStoreFileThumb model=new JinianNet.KuaiBlog.Entity.FileStoreFileThumb();
			DataTable dt=Helper.ExecuteTable(CommandType.Text,strSql.ToString(),parameters);
			if(dt.Rows.Count>0)
			{
				return DataRowToModel(dt.Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public JinianNet.KuaiBlog.Entity.FileStoreFileThumb DataRowToModel(DataRow row)
		{
			JinianNet.KuaiBlog.Entity.FileStoreFileThumb model=new JinianNet.KuaiBlog.Entity.FileStoreFileThumb();
			if (row != null)
			{
				if(row["thumbnailId"]!=null && row["thumbnailId"].ToString()!="")
				{
					model.thumbnailId= new Guid(row["thumbnailId"].ToString());
				}
				if(row["FileId"]!=null && row["FileId"].ToString()!="")
				{
					model.FileId= new Guid(row["FileId"].ToString());
				}
				if(row["size"]!=null && row["size"].ToString()!="")
				{
					model.size=int.Parse(row["size"].ToString());
				}
				if(row["contents"]!=null && row["contents"].ToString()!="")
				{
					model.contents=(byte[])row["contents"];
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataTable GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select thumbnailId,FileId,size,contents ");
			strSql.Append(" FROM be_FileStoreFileThumbs ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return Helper.ExecuteTable(CommandType.Text,strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataTable GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" thumbnailId,FileId,size,contents ");
			strSql.Append(" FROM be_FileStoreFileThumbs ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return Helper.ExecuteTable(CommandType.Text,strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM be_FileStoreFileThumbs ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = Helper.ExecuteScalar(CommandType.Text,strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataTable GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.thumbnailId desc");
			}
			strSql.Append(")AS Row, T.*  from be_FileStoreFileThumbs T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return Helper.ExecuteTable(CommandType.Text,strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataTable GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "be_FileStoreFileThumbs";
			parameters[1].Value = "thumbnailId";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		
		

		
	}
}

