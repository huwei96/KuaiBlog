﻿
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;

namespace JinianNet.KuaiBlog.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:PostComment
	/// </summary>
    public partial class PostComment : BaseDAL
	{
		public PostComment()
		{}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(JinianNet.KuaiBlog.Entity.PostComment model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into be_PostComment(");
			strSql.Append("BlogId,PostCommentId,PostId,ParentCommentId,CommentDate,Author,Email,Website,Comment,Country,Ip,IsApproved,ModeratedBy,Avatar,IsSpam,IsDeleted)");
			strSql.Append(" values (");
			strSql.Append("@BlogId,@PostCommentId,@PostId,@ParentCommentId,@CommentDate,@Author,@Email,@Website,@Comment,@Country,@Ip,@IsApproved,@ModeratedBy,@Avatar,@IsSpam,@IsDeleted)");
			strSql.Append(";select @@IdENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@BlogId", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@PostCommentId", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@PostId", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@ParentCommentId", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@CommentDate", SqlDbType.DateTime),
					new SqlParameter("@Author", SqlDbType.NVarChar,255),
					new SqlParameter("@Email", SqlDbType.NVarChar,255),
					new SqlParameter("@Website", SqlDbType.NVarChar,255),
					new SqlParameter("@Comment", SqlDbType.NVarChar,-1),
					new SqlParameter("@Country", SqlDbType.NVarChar,255),
					new SqlParameter("@Ip", SqlDbType.NVarChar,50),
					new SqlParameter("@IsApproved", SqlDbType.Bit,1),
					new SqlParameter("@ModeratedBy", SqlDbType.NVarChar,100),
					new SqlParameter("@Avatar", SqlDbType.NVarChar,255),
					new SqlParameter("@IsSpam", SqlDbType.Bit,1),
					new SqlParameter("@IsDeleted", SqlDbType.Bit,1)};
			parameters[0].Value = Guid.NewGuid();
			parameters[1].Value = Guid.NewGuid();
			parameters[2].Value = Guid.NewGuid();
			parameters[3].Value = Guid.NewGuid();
			parameters[4].Value = model.CommentDate;
			parameters[5].Value = model.Author;
			parameters[6].Value = model.Email;
			parameters[7].Value = model.Website;
			parameters[8].Value = model.Comment;
			parameters[9].Value = model.Country;
			parameters[10].Value = model.Ip;
			parameters[11].Value = model.IsApproved;
			parameters[12].Value = model.ModeratedBy;
			parameters[13].Value = model.Avatar;
			parameters[14].Value = model.IsSpam;
			parameters[15].Value = model.IsDeleted;

			object obj = Helper.ExecuteScalar(CommandType.Text,strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(JinianNet.KuaiBlog.Entity.PostComment model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update be_PostComment set ");
			strSql.Append("BlogId=@BlogId,");
			strSql.Append("PostCommentId=@PostCommentId,");
			strSql.Append("PostId=@PostId,");
			strSql.Append("ParentCommentId=@ParentCommentId,");
			strSql.Append("CommentDate=@CommentDate,");
			strSql.Append("Author=@Author,");
			strSql.Append("Email=@Email,");
			strSql.Append("Website=@Website,");
			strSql.Append("Comment=@Comment,");
			strSql.Append("Country=@Country,");
			strSql.Append("Ip=@Ip,");
			strSql.Append("IsApproved=@IsApproved,");
			strSql.Append("ModeratedBy=@ModeratedBy,");
			strSql.Append("Avatar=@Avatar,");
			strSql.Append("IsSpam=@IsSpam,");
			strSql.Append("IsDeleted=@IsDeleted");
			strSql.Append(" where PostCommentRowId=@PostCommentRowId");
			SqlParameter[] parameters = {
					new SqlParameter("@BlogId", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@PostCommentId", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@PostId", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@ParentCommentId", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@CommentDate", SqlDbType.DateTime),
					new SqlParameter("@Author", SqlDbType.NVarChar,255),
					new SqlParameter("@Email", SqlDbType.NVarChar,255),
					new SqlParameter("@Website", SqlDbType.NVarChar,255),
					new SqlParameter("@Comment", SqlDbType.NVarChar,-1),
					new SqlParameter("@Country", SqlDbType.NVarChar,255),
					new SqlParameter("@Ip", SqlDbType.NVarChar,50),
					new SqlParameter("@IsApproved", SqlDbType.Bit,1),
					new SqlParameter("@ModeratedBy", SqlDbType.NVarChar,100),
					new SqlParameter("@Avatar", SqlDbType.NVarChar,255),
					new SqlParameter("@IsSpam", SqlDbType.Bit,1),
					new SqlParameter("@IsDeleted", SqlDbType.Bit,1),
					new SqlParameter("@PostCommentRowId", SqlDbType.Int,4)};
			parameters[0].Value = model.BlogId;
			parameters[1].Value = model.PostCommentId;
			parameters[2].Value = model.PostId;
			parameters[3].Value = model.ParentCommentId;
			parameters[4].Value = model.CommentDate;
			parameters[5].Value = model.Author;
			parameters[6].Value = model.Email;
			parameters[7].Value = model.Website;
			parameters[8].Value = model.Comment;
			parameters[9].Value = model.Country;
			parameters[10].Value = model.Ip;
			parameters[11].Value = model.IsApproved;
			parameters[12].Value = model.ModeratedBy;
			parameters[13].Value = model.Avatar;
			parameters[14].Value = model.IsSpam;
			parameters[15].Value = model.IsDeleted;
			parameters[16].Value = model.PostCommentRowId;

			int rows=Helper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int PostCommentRowId)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from be_PostComment ");
			strSql.Append(" where PostCommentRowId=@PostCommentRowId");
			SqlParameter[] parameters = {
					new SqlParameter("@PostCommentRowId", SqlDbType.Int,4)
			};
			parameters[0].Value = PostCommentRowId;

			int rows=Helper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string PostCommentRowIdlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from be_PostComment ");
			strSql.Append(" where PostCommentRowId in ("+PostCommentRowIdlist + ")  ");
			int rows=Helper.ExecuteNonQuery(CommandType.Text,strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public JinianNet.KuaiBlog.Entity.PostComment GetItem(int PostCommentRowId)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 PostCommentRowId,BlogId,PostCommentId,PostId,ParentCommentId,CommentDate,Author,Email,Website,Comment,Country,Ip,IsApproved,ModeratedBy,Avatar,IsSpam,IsDeleted from be_PostComment ");
			strSql.Append(" where PostCommentRowId=@PostCommentRowId");
			SqlParameter[] parameters = {
					new SqlParameter("@PostCommentRowId", SqlDbType.Int,4)
			};
			parameters[0].Value = PostCommentRowId;

			JinianNet.KuaiBlog.Entity.PostComment model=new JinianNet.KuaiBlog.Entity.PostComment();
			DataTable dt=Helper.ExecuteTable(CommandType.Text,strSql.ToString(),parameters);
			if(dt.Rows.Count>0)
			{
				return DataRowToModel(dt.Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public JinianNet.KuaiBlog.Entity.PostComment DataRowToModel(DataRow row)
		{
			JinianNet.KuaiBlog.Entity.PostComment model=new JinianNet.KuaiBlog.Entity.PostComment();
			if (row != null)
			{
				if(row["PostCommentRowId"]!=null && row["PostCommentRowId"].ToString()!="")
				{
					model.PostCommentRowId=int.Parse(row["PostCommentRowId"].ToString());
				}
				if(row["BlogId"]!=null && row["BlogId"].ToString()!="")
				{
					model.BlogId= new Guid(row["BlogId"].ToString());
				}
				if(row["PostCommentId"]!=null && row["PostCommentId"].ToString()!="")
				{
					model.PostCommentId= new Guid(row["PostCommentId"].ToString());
				}
				if(row["PostId"]!=null && row["PostId"].ToString()!="")
				{
					model.PostId= new Guid(row["PostId"].ToString());
				}
				if(row["ParentCommentId"]!=null && row["ParentCommentId"].ToString()!="")
				{
					model.ParentCommentId= new Guid(row["ParentCommentId"].ToString());
				}
				if(row["CommentDate"]!=null && row["CommentDate"].ToString()!="")
				{
					model.CommentDate=DateTime.Parse(row["CommentDate"].ToString());
				}
				if(row["Author"]!=null)
				{
					model.Author=row["Author"].ToString();
				}
				if(row["Email"]!=null)
				{
					model.Email=row["Email"].ToString();
				}
				if(row["Website"]!=null)
				{
					model.Website=row["Website"].ToString();
				}
				if(row["Comment"]!=null)
				{
					model.Comment=row["Comment"].ToString();
				}
				if(row["Country"]!=null)
				{
					model.Country=row["Country"].ToString();
				}
				if(row["Ip"]!=null)
				{
					model.Ip=row["Ip"].ToString();
				}
				if(row["IsApproved"]!=null && row["IsApproved"].ToString()!="")
				{
					if((row["IsApproved"].ToString()=="1")||(row["IsApproved"].ToString().ToLower()=="true"))
					{
						model.IsApproved=true;
					}
					else
					{
						model.IsApproved=false;
					}
				}
				if(row["ModeratedBy"]!=null)
				{
					model.ModeratedBy=row["ModeratedBy"].ToString();
				}
				if(row["Avatar"]!=null)
				{
					model.Avatar=row["Avatar"].ToString();
				}
				if(row["IsSpam"]!=null && row["IsSpam"].ToString()!="")
				{
					if((row["IsSpam"].ToString()=="1")||(row["IsSpam"].ToString().ToLower()=="true"))
					{
						model.IsSpam=true;
					}
					else
					{
						model.IsSpam=false;
					}
				}
				if(row["IsDeleted"]!=null && row["IsDeleted"].ToString()!="")
				{
					if((row["IsDeleted"].ToString()=="1")||(row["IsDeleted"].ToString().ToLower()=="true"))
					{
						model.IsDeleted=true;
					}
					else
					{
						model.IsDeleted=false;
					}
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataTable GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select PostCommentRowId,BlogId,PostCommentId,PostId,ParentCommentId,CommentDate,Author,Email,Website,Comment,Country,Ip,IsApproved,ModeratedBy,Avatar,IsSpam,IsDeleted ");
			strSql.Append(" FROM be_PostComment ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return Helper.ExecuteTable(CommandType.Text,strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataTable GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" PostCommentRowId,BlogId,PostCommentId,PostId,ParentCommentId,CommentDate,Author,Email,Website,Comment,Country,Ip,IsApproved,ModeratedBy,Avatar,IsSpam,IsDeleted ");
			strSql.Append(" FROM be_PostComment ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return Helper.ExecuteTable(CommandType.Text,strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM be_PostComment ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = Helper.ExecuteScalar(CommandType.Text,strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataTable GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.PostCommentRowId desc");
			}
			strSql.Append(")AS Row, T.*  from be_PostComment T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return Helper.ExecuteTable(CommandType.Text,strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataTable GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "be_PostComment";
			parameters[1].Value = "PostCommentRowId";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		
		

		
	}
}

