﻿
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;

namespace JinianNet.KuaiBlog.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:RightRoles
	/// </summary>
    public partial class RightRole : BaseDAL
	{
		public RightRole()
		{}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(JinianNet.KuaiBlog.Entity.RightRole model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into be_RightRoles(");
			strSql.Append("BlogId,RightName,Role)");
			strSql.Append(" values (");
			strSql.Append("@BlogId,@RightName,@Role)");
			strSql.Append(";select @@IdENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@BlogId", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@RightName", SqlDbType.NVarChar,100),
					new SqlParameter("@Role", SqlDbType.NVarChar,100)};
			parameters[0].Value = Guid.NewGuid();
			parameters[1].Value = model.RightName;
			parameters[2].Value = model.Role;

			object obj = Helper.ExecuteScalar(CommandType.Text,strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(JinianNet.KuaiBlog.Entity.RightRole model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update be_RightRoles set ");
			strSql.Append("BlogId=@BlogId,");
			strSql.Append("RightName=@RightName,");
			strSql.Append("Role=@Role");
			strSql.Append(" where RightRoleRowId=@RightRoleRowId");
			SqlParameter[] parameters = {
					new SqlParameter("@BlogId", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@RightName", SqlDbType.NVarChar,100),
					new SqlParameter("@Role", SqlDbType.NVarChar,100),
					new SqlParameter("@RightRoleRowId", SqlDbType.Int,4)};
			parameters[0].Value = model.BlogId;
			parameters[1].Value = model.RightName;
			parameters[2].Value = model.Role;
			parameters[3].Value = model.RightRoleRowId;

			int rows=Helper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int RightRoleRowId)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from be_RightRoles ");
			strSql.Append(" where RightRoleRowId=@RightRoleRowId");
			SqlParameter[] parameters = {
					new SqlParameter("@RightRoleRowId", SqlDbType.Int,4)
			};
			parameters[0].Value = RightRoleRowId;

			int rows=Helper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string RightRoleRowIdlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from be_RightRoles ");
			strSql.Append(" where RightRoleRowId in ("+RightRoleRowIdlist + ")  ");
			int rows=Helper.ExecuteNonQuery(CommandType.Text,strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public JinianNet.KuaiBlog.Entity.RightRole GetItem(int RightRoleRowId)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 RightRoleRowId,BlogId,RightName,Role from be_RightRoles ");
			strSql.Append(" where RightRoleRowId=@RightRoleRowId");
			SqlParameter[] parameters = {
					new SqlParameter("@RightRoleRowId", SqlDbType.Int,4)
			};
			parameters[0].Value = RightRoleRowId;

			JinianNet.KuaiBlog.Entity.RightRole model=new JinianNet.KuaiBlog.Entity.RightRole();
			DataTable dt=Helper.ExecuteTable(CommandType.Text,strSql.ToString(),parameters);
			if(dt.Rows.Count>0)
			{
				return DataRowToModel(dt.Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public JinianNet.KuaiBlog.Entity.RightRole DataRowToModel(DataRow row)
		{
			JinianNet.KuaiBlog.Entity.RightRole model=new JinianNet.KuaiBlog.Entity.RightRole();
			if (row != null)
			{
				if(row["RightRoleRowId"]!=null && row["RightRoleRowId"].ToString()!="")
				{
					model.RightRoleRowId=int.Parse(row["RightRoleRowId"].ToString());
				}
				if(row["BlogId"]!=null && row["BlogId"].ToString()!="")
				{
					model.BlogId= new Guid(row["BlogId"].ToString());
				}
				if(row["RightName"]!=null)
				{
					model.RightName=row["RightName"].ToString();
				}
				if(row["Role"]!=null)
				{
					model.Role=row["Role"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataTable GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select RightRoleRowId,BlogId,RightName,Role ");
			strSql.Append(" FROM be_RightRoles ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return Helper.ExecuteTable(CommandType.Text,strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataTable GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" RightRoleRowId,BlogId,RightName,Role ");
			strSql.Append(" FROM be_RightRoles ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return Helper.ExecuteTable(CommandType.Text,strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM be_RightRoles ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = Helper.ExecuteScalar(CommandType.Text,strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataTable GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.RightRoleRowId desc");
			}
			strSql.Append(")AS Row, T.*  from be_RightRoles T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return Helper.ExecuteTable(CommandType.Text,strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataTable GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "be_RightRoles";
			parameters[1].Value = "RightRoleRowId";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		
		

		
	}
}

