﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JinianNet.KuaiBlog
{
    public class TemplateHandler : System.Web.IHttpHandler
    {
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public Dictionary<string, string> Items
        {
            private set;
            get;
        }

        public static TemplateHandler CreateHandler(System.Web.HttpContext context)
        {
            string[] segments = context.Request.Url.Segments;

            if ((segments.Length == 1 && segments[0] == "/") || (segments.Length > 1 && (segments[1] == "post/" || segments[1] == "category/" || segments[1] == "index.aspx")))
            {
                TemplateHandler handler = new TemplateHandler();
                handler.Items = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);
                if (segments.Length > 1)
                {
                    switch (segments[1].Trim('/'))
                    {
                        case "post":
                            handler.Items.Add("type", "post");
                            handler.Items.Add("slug", System.Web.HttpUtility.UrlDecode(segments[segments.Length - 1].Substring(0, segments[segments.Length - 1].LastIndexOf('.'))));
                            break;
                        case "page":
                            handler.Items.Add("type", "page");
                            handler.Items.Add("slug", System.Web.HttpUtility.UrlDecode(segments[segments.Length - 1].Substring(0, segments[segments.Length - 1].LastIndexOf('.'))));
                            break;
                        case "category":
                            handler.Items.Add("type", "category");
                            handler.Items.Add("slug", System.Web.HttpUtility.UrlDecode(segments[segments.Length - 1].Substring(0, segments[segments.Length - 1].LastIndexOf('.'))));
                            handler.Items.Add("index", context.Request.QueryString["page"] ?? "1");
                            handler.Items.Add("size", "20");
                            break;
                        case "index.aspx":
                            handler.Items.Add("type", "index");
                            break;
                        default:
                            return null;
                    }
                }
                else
                {
                    handler.Items.Add("type", "index");
                }

                return handler;

            }

            return null;
        }

        public void ProcessRequest(System.Web.HttpContext context)
        {
            Generate generate = new Generate(context);
            switch (this.Items["type"])
            {
                case "category":
                    generate.GeneratePostList(this.Items["slug"]);
                    break;
                case "post":
                    generate.GeneratePost(this.Items["slug"]);
                    break;
                case "page":
                    generate.GeneratePage(this.Items["slug"]);
                    break;
                case "index":
                case "default":
                    generate.GetDefault();
                    break;
                default:
                    context.Response.Clear();
                    context.Response.StatusCode = 404;
                    break;
            }
        }
    }
}
